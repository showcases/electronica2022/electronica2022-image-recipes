## Flashing image

Download the latest CI artifacts for the [LE Audio central](https://gitlab.collabora.com/showcases/electronica2022/electronica2022-image-recipes/-/jobs/artifacts/main/download?job=build-image-leaudio-central) and [LE Audio peripheral](https://gitlab.collabora.com/showcases/electronica2022/electronica2022-image-recipes/-/jobs/artifacts/main/download?job=build-image-leaudio-peripheral) and unzip the contents

### Flash to USB drive

#### LE Audio central

```
$ sudo bmaptool copy out/apertis-v2023pre-leaudio-central-amd64.img.gz /dev/<DEVICE>
```

Insert the USB drive  and power the board.

The board should boot to the Apertis graphical interface on the HDMI screen.

#### LE Audio peripheral

```
$ sudo bmaptool copy out/apertis-v2023pre-leaudio-peripheral-amd64.img.gz /dev/<DEVICE>
```

Insert the USB drive  and power the board.

The board should boot to a Linux console on the HDMI screen and be ready for LE Audio BAP connection.
