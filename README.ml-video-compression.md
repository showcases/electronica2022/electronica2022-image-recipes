# ML Video Compression

The demo is designed to run on the [Renegate Elite Rockchip RK3399](https://libre.computer/products/roc-rk3399-pc/) board.

## Known problems

### Power
Problem: Powering from USB-C Power Delivery adapter (e.g. Dell Laptop adapter), 12V from
pin headers or PoE using the shield does not work.

Workaround: Use a 5V USB-C power adapter which does no voltage negotiation e.g. the
[Raspberry Pi USB-C adapter](https://www.amazon.co.uk/Official-Raspberry-Pi-Type-C-Adapter/dp/B07Y1S37R7).


## Flashing image

Download the [latest CI artifacts for the main branch](https://gitlab.collabora.com/showcases/electronica2022/electronica2022-image-recipes/-/jobs/artifacts/main/download?job=build%20image%20ml-video-compression)
and unzip the contents

### Flash to SD card

```
$ sudo bmaptool copy out/apertis-ml-video-compression-roc-pc-rk3399.img.gz /dev/<DEVICE>
```

Insert the SD card into the SD slot and power the board up by applying 5V to the
USB-C socket labelled `TYPEC0` (the one next to the HDMI socket).

The board should boot to a Linux console on the HDMI screen.


## Access the Device

### HDMI output access

The demo is set to autostart when it detects a camera is connected. No interaction should be required.

### Serial console access

Debug pins are next to the `TYPEC1` socket. Pins are: 1 UART2_RXD, 2 UART2_TXD, 3 Ground (where pin1 is the closest to `TYPEC0`)

Required serial parameters:
- Baud rate: 1,500,000
- Data bit: 8
- Stop bit: 1
- Parity check: none
- Flow control: none

### SSH access

There is a SSH service listening (Ethernet configured for DHCP). Should be discoverable on the local network with `electronica2022-rk3399` as the hostname.

### Default user

Generated user is username: `user`, password: `user`
